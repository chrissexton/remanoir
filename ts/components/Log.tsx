import * as React from "react";

export interface LogProps { entry: any }

// 'LogProps' describes the shape of props.
// State is never set so we use the 'undefined' type.
export class Log extends React.Component<LogProps, undefined> {
	render() {
		return (
<div className="mdc-card demo-card">
  <section className="mdc-card__primary">
	<h1 className="mdc-card__title mdc-card__title--large">{this.props.entry.title}</h1>
	<h2 className="mdc-card__subtitle">{this.props.entry.timestamp}</h2>
  </section>
  <section className="mdc-card__supporting-text">
	{this.props.entry.description}
  </section>
  <section className="mdc-card__actions">
	<button className="mdc-button mdc-button--compact mdc-card__action">Action 1</button>
	<button className="mdc-button mdc-button--compact mdc-card__action">Action 2</button>
  </section>
</div>
		);
	}
}

