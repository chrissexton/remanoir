import * as React from "react";

export interface AddButtonProps { }

// 'AddButtonProps' describes the shape of props.
// State is never set so we use the 'undefined' type.
export class AddButton extends React.Component<AddButtonProps, undefined> {
	render() {
		return (
			<button id="demo-absolute-fab" className="mdc-fab mdc-ripple-upgraded mdc-ripple-upgraded--background-active-fill mdc-ripple-upgraded--foreground-activation myFAB">
				<span className="mdc-fab__icon">
					<i className="material-icons">add</i>
				</span>
			</button>
		);
	}
}
