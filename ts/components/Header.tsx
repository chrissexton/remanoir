import * as React from "react";

export interface HeaderProps { itemName: string; }

export class Header extends React.Component<HeaderProps, undefined> {
	public render() {
		return (
    <header className="mdc-toolbar mdc-toolbar--fixed">
      <div className="mdc-toolbar__row">
        <section className="mdc-toolbar__section mdc-toolbar__section--align-start">
          <a className="material-icons">menu</a>
          <span className="mdc-toolbar__title">{this.props.itemName}</span>
        </section>
        <section className="mdc-toolbar__section mdc-toolbar__section--align-end" role="toolbar">
          <a className="material-icons" aria-label="Download" alt="Download">file_download</a>
          <a className="material-icons" aria-label="Print this page" alt="Print this page">print</a>
          <a className="material-icons" aria-label="Bookmark this page" alt="Bookmark this page">bookmark</a>
        </section>
      </div>
    </header>
		);
	}
}

