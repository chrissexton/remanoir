import * as React from "react";

import { Hello } from "./Hello";
import { Description } from "./Description";
import { AddButton } from "./AddButton";

export interface MainProps { item: any }

export class Main extends React.Component<MainProps, undefined> {
	public render() {
		return (
		  <main>
			  <div className="mdc-toolbar-fixed-adjust">
				  <Description item={this.props.item} />
			  </div>
			  <AddButton />
		  </main>
		);
	}
}
