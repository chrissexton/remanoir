import * as React from "react";

export interface HelloProps { compiler: string; framework: string; }

// 'HelloProps' describes the shape of props.
// State is never set so we use the 'undefined' type.
export class Hello extends React.Component<HelloProps, undefined> {
	render() {
		return (
		<div>
			<h1>Hello from {this.props.compiler} and {this.props.framework}!</h1>
			<h2 className="mdc-typography--display2">Hello, Material Components!</h2>
			<div className="mdc-textfield" data-mdc-auto-init="MDCTextfield">
			  <input type="text" className="mdc-textfield__input" id="demo-input" />
			  <label htmlFor="demo-input" className="mdc-textfield__label">Tell us how you feel!</label>
			</div>
		</div>
		);
	}
}
