import * as React from "react";
import * as ReactDOM from "react-dom";
import * as printf from "printf";
import { structs } from "./structs.js";

import { Header } from "./components/Header";
import { Main } from "./components/Main";

let getData = function(user, id, callback) {
	let url = printf("/user/%s/id/%s.proto", user, id);
	let xhr = new XMLHttpRequest();
	let async = true;
	let dataType = "arraybuffer";

	xhr.addEventListener('load', function(){
		callback(new Uint8Array(xhr.response));
	});

	xhr.responseType = dataType as XMLHttpRequestResponseType;
	xhr.open("GET", url, async);
	xhr.send(null);
}

getData("cws", "100", function(data) {
	var item = structs.Item.decode(data);
	console.log(item);
	ReactDOM.render(
		<div><Header itemName={item.name} /><Main item={item} /></div>,
		document.getElementById("root")
	);
});

