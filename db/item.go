package db

import (
	"github.com/golang/protobuf/proto"
	"gitlab.com/chrissexton/remanoir/structs"
)

func (d *DB) WriteBinaryItem(user, name string, itemProto []byte) error {
	tx, err := d.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()
	b := tx.Bucket([]byte(items))

	if err := b.Put([]byte(name), itemProto); err != nil {
		return err
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func (d *DB) WriteItem(user, name string, item structs.Item) error {
	itemProto, err := proto.Marshal(&item)
	if err != nil {
		return err
	}
	return d.WriteBinaryItem(user, name, itemProto)
}

func (d *DB) GetBinaryItem(user, name string) ([]byte, error) {
	tx, err := d.Begin(true)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	b := tx.Bucket([]byte(items))
	protoItem := b.Get([]byte(name))

	if protoItem == nil {
		return nil, nil
	}

	if err := tx.Commit(); err != nil {
		return nil, err
	}

	return protoItem, nil
}

func (d *DB) GetItem(user, name string) (structs.Item, error) {
	protoItem, err := d.GetBinaryItem(user, name)
	if err != nil {
		return structs.NilItem, err
	}

	var item structs.Item
	if err := proto.Unmarshal(protoItem, &item); err != nil {
		return structs.NilItem, err
	}

	return item, nil
}
