package db

import (
	"os"
	"testing"

	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"

	"gitlab.com/chrissexton/remanoir/structs"
)

const (
	testDB = "testing.db"
)

func getDB(t *testing.T) *DB {
	return OpenDB(testDB)
}

func cleanup(t *testing.T, db *DB) {
	if err := db.Close(); err != nil {
		t.Fatal(err)
	}

	if err := os.Remove(testDB); err != nil {
		t.Fatal(err)
	}
}

func TestWriteAccount(t *testing.T) {
	db := getDB(t)
	defer cleanup(t, db)

	expected := structs.Account{
		Name:  "test",
		Email: "test@test.com",
	}

	err := db.WriteAccount(expected)
	if err != nil {
		t.Fatal(err)
	}

	if err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("accounts"))
		v := b.Get([]byte("test"))
		var actual structs.Account
		err := proto.Unmarshal(v, &actual)
		if err != nil {
			t.Fatal(err)
		}
		if actual != expected {
			t.Fatalf("%+v != %+v", actual, expected)
		}
		return nil
	}); err != nil {
		t.Fatal(err)
	}
}

func TestGetAccount(t *testing.T) {
	db := getDB(t)
	defer cleanup(t, db)

	expected := structs.Account{
		Name:  "test",
		Email: "test@test.com",
	}

	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("accounts"))
		expectedProto, err := proto.Marshal(&expected)
		if err != nil {
			return err
		}
		err = b.Put([]byte("test"), expectedProto)
		return err
	})
	if err != nil {
		t.Fatal(err)
	}

	actual, err := db.GetAccount("test")
	if err != nil {
		t.Fatal(err)
	}

	if actual != expected {
		t.Fatalf("%+v != %+v", actual, expected)
	}

}

func TestGetWrongAccount(t *testing.T) {
	db := getDB(t)
	defer cleanup(t, db)

	account := structs.Account{
		Name:  "test",
		Email: "test@test.com",
	}

	if err := db.WriteAccount(account); err != nil {
		t.Fatal(err)
	}

	actual, err := db.GetAccount("test2")
	if err != nil {
		t.Fatal(err)
	}

	if actual == account {
		t.Errorf("%+v == %+v and it shouldn't", actual, account)
	}

	if actual.Name != "" || actual.Email != "" {
		t.Errorf("actual: %+v should be empty", actual)
	}
}
