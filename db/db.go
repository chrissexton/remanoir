package db

import (
	"log"

	"github.com/boltdb/bolt"
)

const (
	accounts = "accounts"
	items    = "items"

	dbFile = "my.db"
)

var names = []string{accounts, items}

type DB struct {
	*bolt.DB
}

func OpenDB(path string) *DB {
	d, err := bolt.Open(path, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	db := &DB{d}

	db.createBuckets(names)

	return db
}

func (d *DB) createBuckets(bucketNames []string) error {
	tx, err := d.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	for _, name := range bucketNames {
		if _, err := tx.CreateBucketIfNotExists([]byte(name)); err != nil {
			return err
		}
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}
