package db

import (
	"github.com/golang/protobuf/proto"
	"gitlab.com/chrissexton/remanoir/structs"
)

func (d *DB) WriteAccount(account structs.Account) error {
	tx, err := d.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()
	b := tx.Bucket([]byte(accounts))

	accountProto, err := proto.Marshal(&account)
	if err != nil {
		return err
	}

	if err := b.Put([]byte(account.Name), accountProto); err != nil {
		return err
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func (d *DB) GetAccount(name string) (structs.Account, error) {
	tx, err := d.Begin(true)
	if err != nil {
		return structs.NilAccount, err
	}
	defer tx.Rollback()

	b := tx.Bucket([]byte(accounts))
	protoAccount := b.Get([]byte(name))

	if protoAccount == nil {
		return structs.NilAccount, nil
	}

	if err := tx.Commit(); err != nil {
		return structs.NilAccount, err
	}

	var account structs.Account
	if err := proto.Unmarshal(protoAccount, &account); err != nil {
		return structs.NilAccount, err
	}

	return account, nil
}
