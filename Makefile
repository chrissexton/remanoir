all: remanoir assets/bundle.js

.PHONY: remanoir serve test clean npm

test: remanoir
	go test ./...

remanoir:
	go generate ./...
	go build

serve: remanoir assets/bundle.js
	./remanoir

clean:
	@rm -rf remanoir structs/structs.pb.go build
	@rm -rf node_modules structs.js structs.d.ts main.js bundle.js

build: remanoir
	mkdir -p build/assets
	cp remanoir build
	cp assets/*.html build/assets
	cp assets/bundle.js build/assets
	cp assets/material-components-web.min.css build/assets
	cp assets/material-components-web.min.js build/assets

npm:
	npm install

assets/bundle.js: npm ts/main.ts ts/structs.d.ts
	./node_modules/.bin/webpack

ts/structs.js: structs/structs.proto
	./node_modules/.bin/pbjs -t static-module -w commonjs -o $@ $?

ts/structs.d.ts: ts/structs.js
	./node_modules/.bin/pbts -o $@ $?
