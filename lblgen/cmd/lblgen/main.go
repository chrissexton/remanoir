package main

import (
	"io/ioutil"
	"log"

	"gitlab.com/chrissexton/remanoir/lblgen"
)

func main() {
	c := lblgen.DefaultConfig
	barcodes, err, seed := lblgen.GenCodes(c, 4*6)
	if err != nil {
		log.Fatal(err)
	}
	buf, err := lblgen.MkPDF(c, seed, barcodes)
	if err != nil {
		log.Fatal(err)
	}
	if err := ioutil.WriteFile("qrcodes.pdf", buf.Bytes(), 0666); err != nil {
		log.Fatal(err)
	}
}
