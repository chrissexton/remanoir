package lblgen

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/url"
	"strconv"

	"bitbucket.org/zombiezen/gopdf/pdf"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"
	"github.com/satori/go.uuid"
)

type Config struct {
	szX        pdf.Unit
	szY        pdf.Unit
	paddingX   pdf.Unit
	paddingY   pdf.Unit
	leftMargin pdf.Unit
	topMargin  pdf.Unit
	rezX       int
	rezY       int
	cols       float32
	rows       float32
	User       string
	URL        string
	Namespace  uuid.UUID
}

var DefaultConfig = Config{
	szX:        1.5 * pdf.Inch,
	szY:        1.5 * pdf.Inch,
	paddingX:   0.375 * pdf.Inch,  // 6/16=0.375
	paddingY:   0.25 * pdf.Inch,   // 4/16 = 0.25
	leftMargin: 0.6875 * pdf.Inch, // 11/16 = 0.6875
	topMargin:  0.375 * pdf.Inch,  // 6/16=0.375
	rezX:       800,
	rezY:       800,
	cols:       4,
	rows:       6,
	User:       "cws",
	URL:        "https://qr.chrissexton.org/user/cws/id/",
	Namespace:  uuid.FromStringOrNil("4e58d5e9-5924-4b55-93d2-e2dba9c89d1d"),
}

func mkAddr(config Config, seed int64) string {
	name := strconv.FormatFloat(rand.Float64(), 'E', -1, 64)
	uid := uuid.NewV5(config.Namespace, name)
	u, _ := url.Parse(config.URL)
	u.Path += uid.String()
	q := u.Query()
	q.Set("s", strconv.FormatInt(seed, 10))
	u.RawQuery = q.Encode()
	return u.String()
}

func mkQR(value string, szX, szY int) (barcode.Barcode, error) {
	qrcode, err := qr.Encode(value, qr.Q, qr.Auto)
	if err != nil {
		return nil, err
	} else {
		qrcode, err = barcode.Scale(qrcode, szX, szY)
		if err != nil {
			return nil, err
		}
	}
	return qrcode, nil
}

func GenCodesWithUUIDS(config Config, uuids []string) ([]barcode.Barcode, error) {
	codes := []barcode.Barcode{}
	for _, uuid := range uuids {
		tag, err := json.Marshal(uuid)
		if err != nil {
			return nil, err
		}
		code, err := mkQR(string(tag), config.rezX, config.rezY)
		if err != nil {
			return nil, err
		}
		codes = append(codes, code)
	}
	return codes, nil
}

func GenCodes(config Config, n int) ([]barcode.Barcode, error, int64) {
	seed := rand.Int63()
	return GenCodesWithSeed(config, n, seed)
}

func GenCodesWithSeed(config Config, n int, seed int64) ([]barcode.Barcode, error, int64) {
	rand.Seed(seed)
	var uuids []string
	for i := 0; i < n; i++ {
		uuids = append(uuids, mkAddr(config, seed))
	}
	barcodes, err := GenCodesWithUUIDS(config, uuids)
	return barcodes, err, seed
}

func MkPDF(config Config, seed int64, codes []barcode.Barcode) (bytes.Buffer, error) {
	doc := pdf.New()
	canvas := doc.NewPage(pdf.USLetterWidth, pdf.USLetterHeight)

	paddingX, paddingY := config.paddingX, config.paddingY
	leftMargin, topMargin := config.leftMargin, config.topMargin
	cX, cY := leftMargin, topMargin

	fmt.Printf("Margins: %0.3f %0.3f\n", leftMargin, topMargin)
	fmt.Printf("Padding: %0.3f %0.3f\n", paddingX, paddingY)

	n := len(codes)
	szX := config.szX
	szY := config.szY

	for i := 0; i < n; i++ {
		src := codes[i]
		b := pdf.Rectangle{
			Min: pdf.Point{
				X: cX,
				Y: cY,
			},
			Max: pdf.Point{
				X: szX + cX,
				Y: szY + cY,
			},
		}
		log.Printf("%d-th: %s", i+1, b)
		canvas.DrawImage(src, b)

		if i%4 == 3 {
			cX = leftMargin
			cY += szY + paddingY
		} else {
			cX += szX + paddingX
		}
	}

	text := new(pdf.Text)
	text.SetFont(pdf.Helvetica, 10)
	text.Text(fmt.Sprintf("Avery 22805 - %s - %d", config.User, seed))
	canvas.Translate(0.25*pdf.Inch, 0.125*pdf.Inch)
	canvas.DrawText(text)
	canvas.Translate(0, 0)

	canvas.Close()

	var buf bytes.Buffer

	if err := doc.Encode(&buf); err != nil {
		return buf, err
	}

	return buf, nil
}
