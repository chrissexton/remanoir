package main

import (
	"flag"
	"log"
	"net/http"

	"gitlab.com/chrissexton/remanoir/handlers"

	"github.com/gorilla/mux"
)

var addr = flag.String("addr", "127.0.0.1:8080", "Address to serve")

func main() {
	flag.Parse()

	r := mux.NewRouter()
	r.HandleFunc("/", handlers.HomeHandler)
	r.HandleFunc("/user/{user}/id/{id}.proto/savedummy", handlers.ItemProtoSaveDummyHandler)
	r.HandleFunc("/user/{user}/id/{id}.proto/save", handlers.ItemProtoSaveHandler)
	r.HandleFunc("/user/{user}/id/{id}.proto", handlers.ItemProtoHandler)
	r.HandleFunc("/user/{user}/id/{id}", handlers.ItemHandler)
	r.HandleFunc("/user/{user}/gencodes", handlers.GenCodesHandler)
	r.HandleFunc("/user/{user}/gencodes/id/{id}", handlers.GenCodesHandler)
	r.HandleFunc("/error", handlers.ErrorHandler)
	r.PathPrefix("/assets/").Handler(http.StripPrefix("/assets/", http.FileServer(http.Dir("assets"))))

	log.Printf("Serving on http://%s", *addr)
	log.Fatal(http.ListenAndServe(*addr, r))
}
