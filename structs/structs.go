package structs

import uuid "github.com/satori/go.uuid"

//go:generate protoc --go_out=. structs.proto

var NilItem = Item{}
var NilAccount = Account{}

func (i Item) UUID() uuid.UUID {
	return uuid.FromStringOrNil(i.Id)
}
