package handlers

import (
	"bytes"
	"log"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/chrissexton/remanoir/lblgen"
)

func GenCodesHandler(w http.ResponseWriter, r *http.Request) {
	seed := getField(r, "id")
	user := getField(r, "user")
	var pdf bytes.Buffer
	c := lblgen.DefaultConfig
	c.User = user
	if seed == "" {
		// get without seed
		barcodes, err, seed := lblgen.GenCodes(c, 4*6)
		if err != nil {
			http.Redirect(w, r, "/error", 500)
			return
		}
		pdf, err = lblgen.MkPDF(c, seed, barcodes)
		if err != nil {
			http.Redirect(w, r, "/error", 500)
			return
		}
	} else {
		// get with seed
		intSeed, err := strconv.ParseInt(seed, 10, 64)
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, "/error", 500)
			return
		}
		barcodes, err, seed := lblgen.GenCodesWithSeed(c, 4*6, intSeed)
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, "/error", 500)
			return
		}
		pdf, err = lblgen.MkPDF(c, seed, barcodes)
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, "/error", 500)
			return
		}
	}

	// set content-type
	// return buffer of PDF
	http.ServeContent(w, r, "qrcodes.pdf", time.Now(), bytes.NewReader(pdf.Bytes()))
}
