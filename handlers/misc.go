package handlers

import (
	"flag"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/chrissexton/remanoir/db"
	"gitlab.com/chrissexton/remanoir/structs"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/mux"
)

var dbFile = flag.String("dbFile", "qr.db", "Path to DB file")
var cacheTpls = flag.Bool("cacheTpls", false, "Cache templates (use false for development)")

var tpls = make(map[string]*template.Template)

var loggedInDeps = []string{
	"assets/sidebar.html",
	"assets/header.html",
	"assets/footer.html",
}

var tplDeps = map[string][]string{}

func addDeps(name, tpl string, deps []string) {
	tplDeps[name] = []string{tpl}
	tplDeps[name] = append(tplDeps[name], deps...)
}

func init() {
	addDeps("item", "assets/item.html", loggedInDeps)
	addDeps("newItem", "assets/newItem.html", loggedInDeps)
}

type PageOpts struct {
	UseTabs bool
}

var useTabs = PageOpts{true}

func getTpl(path string) (*template.Template, error) {
	var err error
	tpl, ok := tpls[path]
	if !ok || !*cacheTpls {
		log.Printf("Getting template files for %s", path)
		tpl, err = template.ParseFiles(tplDeps[path]...)
		if err != nil {
			return nil, err
		}
		tpls[path] = tpl
	}
	return tpl, nil
}

func exTpl(w http.ResponseWriter, r *http.Request, path string, data interface{}) {
	tpl, err := getTpl(path)
	if err != nil {
		log.Println(err)
		ErrorHandler(w, r)
	}
	if err := tpl.Execute(w, data); err != nil {
		log.Println(err)
		ErrorHandler(w, r)
	}
}

func openDB() *db.DB {
	return db.OpenDB(*dbFile)
}

func getField(r *http.Request, field string) string {
	vars := mux.Vars(r)
	id := vars[field]
	return id
}

func ErrorHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte("something is technically wrong"))
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Hit HomeHandler")
	exTpl(w, r, "item", struct {
		PageOpts
		Item structs.Item
	}{PageOpts: useTabs, Item: structs.NilItem})
}

func ItemProtoHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Hit ItemProtoHandler")
	id := getField(r, "id")
	user := getField(r, "user")
	db := openDB()
	defer db.Close()

	it, err := db.GetBinaryItem(user, id)
	if err != nil {
		log.Println(err)
		ErrorHandler(w, r)
		return
	}

	var item structs.Item
	err = proto.Unmarshal(it, &item)
	if err != nil {
		log.Println(err)
	}
	log.Println(item)

	w.Header().Set("Content-Type", "application/octet-stream")
	w.WriteHeader(http.StatusOK)
	w.Write(it)
}

func ItemProtoSaveDummyHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Hit ItemProtoSaveHandler")
	id := getField(r, "id")
	user := getField(r, "user")
	db := openDB()
	defer db.Close()

	item := structs.Item{
		Id:          id,
		Name:        "Test Item",
		Description: "This is a test item.",
		Type:        3,
	}

	err := db.WriteItem(user, id, item)
	if err != nil {
		log.Println(err)
		ErrorHandler(w, r)
		return
	}

	var itemProto []byte
	itemProto, err = proto.Marshal(&item)
	if err != nil {
		log.Println(err)
		ErrorHandler(w, r)
		return
	}

	w.Header().Set("Content-Type", "application/octet-stream")
	w.WriteHeader(http.StatusOK)
	w.Write(itemProto)
}

func ItemProtoSaveHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Hit ItemProtoSaveHandler")
	id := getField(r, "id")
	user := getField(r, "user")
	db := openDB()
	defer db.Close()

	// Accept whatever payload was POSTed and write to DB
	itemProto, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		ErrorHandler(w, r)
		return
	}

	err = db.WriteBinaryItem(user, id, itemProto)
	if err != nil {
		log.Println(err)
		ErrorHandler(w, r)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func ItemHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Hit ItemHandler")
	id := getField(r, "id")
	user := getField(r, "user")
	db := openDB()
	defer db.Close()

	it, err := db.GetItem(user, id)
	if err != nil {
		ErrorHandler(w, r)
	}

	log.Printf("it: %s == nil: %s: %v", it.String(), structs.NilItem.String(), it.String() == structs.NilItem.String())
	if it.String() != structs.NilItem.String() {
		exTpl(w, r, "item", struct {
			PageOpts
			Item structs.Item
		}{PageOpts: useTabs, Item: it})
	} else {
		log.Println("Choosing newItem")
		data := struct {
			PageOpts
			Id   string
			User string
		}{Id: id, User: user}
		log.Printf("Passing to the template:\n%+v", data)
		exTpl(w, r, "newItem", data)
	}
}
